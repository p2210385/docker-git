# Guide d'utilisation du TP Docker Git

Ce guide fournit des instructions étape par étape pour configurer et tester le TP Docker Git.

## Prérequis

Assurez-vous d'avoir installé Docker sur votre système.

## Étapes pour tester le TP Docker Git

1. **Cloner le référentiel Git :**

##Contenu du Dockerfile pour le serveur Git :

##Créez un fichier nommé Dockerfile.git-server avec le contenu suivant :

FROM ubuntu-yacine

RUN apt-get update && apt-get install -y git
RUN useradd -m -d /home/git -s /bin/bash git
H
EXPOSE 22

COPY bob_rsa.pub /home/git/.ssh/bob_rsa.pub
COPY alice_rsa.pub /home/git/.ssh/alice_rsa.pub
RUN cat /home/git/.ssh/bob_rsa.pub /home/git/.ssh/alice_rsa.pub >> /home/git/.ssh/authorized_keys

RUN chmod 600 /home/git/.ssh/authorized_keys
CMD ["/usr/sbin/sshd", "-D"]


##Contenu du Dockerfile pour Git-client :

##Créez un fichier nommé Dockerfile.git-client avec le contenu suivant :

FROM ubuntu-yacine

RUN apt-get update && apt-get install -y git
RUN useradd -m -d /home/bob -s /bin/bash bob
RUN useradd -m -d /home/alice -s /bin/bash alice

COPY bob_rsa /home/bob/.ssh/bob_rsa
COPY alice_rsa /home/alice/.ssh/alice_rsa

RUN chmod 600 /home/bob/.ssh/bob_rsa
RUN chmod 600 /home/alice/.ssh/alice_rsa

EXPOSE 22

CMD ["/usr/sbin/sshd", "-D"]




##Contenu du fichier docker-compose.yml :

##Créez un fichier nommé docker-compose.yml avec le contenu suivant :

version: '3'
services:
  git-server:
    build:
      context: .
      dockerfile: Dockerfile.git-server
    container_name: git-server
    hostname: git-server
    networks:
      git-network:
        ipv4_address: 172.18.0.15
    ports:
      - "2222:22"
    restart: always

  git-bob:
    build:
      context: .
      dockerfile: Dockerfile.git-client
    container_name: git-bob
    hostname: git-bob
    networks:
      git-network:
        ipv4_address: 172.18.0.16
    restart: always

  git-alice:
    build:
      context: .
      dockerfile: Dockerfile.git-client
    container_name: git-alice
    hostname: git-alice
    networks:
      git-network:
        ipv4_address: 172.18.0.17
    restart: always

networks:
  git-network:
    driver: bridge
    ipam:
      config:
        - subnet: 172.18.0.0/16

##Démarrer les conteneurs Docker :

docker-compose up -d

##Vérifier l'état des conteneurs :

docker-compose ps

##Tester la connexion SSH depuis Git-Bob vers Git-Server :

docker exec -it git-bob ssh -i /home/git/.ssh/bob_rsa git@172.18.0.15

Assurez-vous de remplacer "votre-utilisateur" par votre nom d'utilisateur GitHub

##Remarques
Assurez-vous que les clés SSH sont correctement configurées et que les autorisations sont définies comme décrit dans ce guide.
Si vous rencontrez des problèmes, assurez-vous de vérifier les logs des conteneurs Docker pour diagnostiquer les problèmes éventuels.


Assurez-vous de remplacer `"votre-utilisateur"` par votre nom d'utilisateur GitHub

